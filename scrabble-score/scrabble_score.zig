const std = @import("std");

// Notice: Only engish letters are supported by this program.

pub const ComputationError = error{
    IllegalArgument,
};

fn isUpperCase(c: u8) bool {
    if (c < 'A' or c > 'Z') {
        return false;
    }

    return true;
}

fn isLowerCase(c: u8) bool {
    if (c < 'a' or c > 'z') {
        return false;
    }

    return true;
}

fn isValidChar(c: u8) bool {
    if (!isLowerCase(c) and !isUpperCase(c)) {
        return false;
    }

    return true;
}

fn toLower(c: u8) ComputationError!u8 {
    if (!isValidChar(c)) {
        return ComputationError.IllegalArgument;
    }

    const diff: u8 = 32; // 'A' - 'a';

    if (isUpperCase(c)) {
        return c + diff;
    }

    return c;
}

fn scoreImpl(s: []const u8) !u32 {
    var map = std.AutoHashMap(u8, u32).init(std.heap.page_allocator);
    defer map.deinit();
    try map.putNoClobber('a', 1);
    try map.putNoClobber('e', 1);
    try map.putNoClobber('i', 1);
    try map.putNoClobber('o', 1);
    try map.putNoClobber('u', 1);
    try map.putNoClobber('l', 1);
    try map.putNoClobber('n', 1);
    try map.putNoClobber('r', 1);
    try map.putNoClobber('s', 1);
    try map.putNoClobber('t', 1);
    try map.putNoClobber('d', 2);
    try map.putNoClobber('g', 2);
    try map.putNoClobber('b', 3);
    try map.putNoClobber('c', 3);
    try map.putNoClobber('m', 3);
    try map.putNoClobber('p', 3);
    try map.putNoClobber('f', 4);
    try map.putNoClobber('h', 4);
    try map.putNoClobber('v', 4);
    try map.putNoClobber('w', 4);
    try map.putNoClobber('y', 4);
    try map.putNoClobber('k', 5);
    try map.putNoClobber('j', 8);
    try map.putNoClobber('x', 8);
    try map.putNoClobber('q', 10);
    try map.putNoClobber('z', 10);

    var result: u32 = 0;
    for (s) |c| {
        var lower_c = toLower(c) catch unreachable;
        var val = map.get(lower_c) orelse return 0;
        result += val;
    }

    return result;
}

pub fn score(s: []const u8) u32 {
    return scoreImpl(s) catch unreachable;
}
