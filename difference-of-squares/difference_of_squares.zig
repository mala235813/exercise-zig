pub fn squareOfSum(number: usize) usize {
    var sum: usize = 0;
    for (0..(number+1)) |i| {
        sum += i;
    }

    var square: usize = sum * sum;
    return square;
}

pub fn sumOfSquares(number: usize) usize {
    var sum: usize = 0;
    for (0..(number+1)) |i| {
        sum += i * i;
    }

    return sum;
}

pub fn differenceOfSquares(number: usize) usize {
    var diff: usize = squareOfSum(number) - sumOfSquares(number);
    return diff;
}
