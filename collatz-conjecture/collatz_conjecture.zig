pub const ComputationError = error{
    IllegalArgument,
};

fn isEven(number: usize) bool {
    return number % 2 == 0;
}

fn nextCollatz(number: usize) usize {
    if (isEven(number)) {
        return number / 2;
    } else {
        return number * 3 + 1;
    }
}

pub fn steps(number: usize) anyerror!usize {
    if (number < 1) {
        return ComputationError.IllegalArgument;
    }

    if (number == 1) {
        return 0;
    }

    var count: usize = 0;
    var collatz = number;
    while (collatz > 1) {
        collatz = nextCollatz(collatz);
        count += 1;
    }

    return count;
}
