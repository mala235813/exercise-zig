pub fn isLeapYear(year: u32) bool {
    // 1. on every year that is evenly divisible by 4
    if (@mod(year, 4) != 0) {
        return false;
    }

    // 3. unless the year is also evenly divisible by 400
    if (@mod(year, 400) == 0) {
        return true;
    }

    // 2. except every year that is evenly divisible by 100
    if (@mod(year, 100) == 0) {
        return false;
    }

    return true;
}
